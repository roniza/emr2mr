#pragma once
// See // https://docs.google.com/document/d/12F97T8ED4JpcgB6JDQNXtMNnvy4e_QbLMIUHH7wPE34/edit
// The following function is required :
// BOOL UnwrapMultiFrameImage(BSTR fileName, int* numberOfFrameImages);
//
//  The function receives path to multi - frame DICOM image and should create single frame images from it.
//  Unwrapped images name will be like the original name with postfix of _{ i } (running index) � to avoid similar name in same directory
//  numberOfFrameImages parameter  should be the number of new images that were unwrapped.
//
//  If the function succeeded to unwrap the multi - Frame image it will return TRUE.
//  If it is not multi - frame, it will do nothing with the file, set numberOfFrameImages to - 1 and return TRUE.
//  The function will return FALSE if it failed unwrap multi - frame file � Roni, what should be done from OnStoreDone if unwrapping fails ?

#include <comutil.h> // For BSTR and _bstr_t

bool UnwrapMultiFrameImage(/*IN*/BSTR filename, /*OUT*/int* numOfFrames);
