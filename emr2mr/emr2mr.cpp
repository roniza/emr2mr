// emr2mr.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Test app for UnwrapMultiFrameImage
// https://docs.google.com/document/d/12F97T8ED4JpcgB6JDQNXtMNnvy4e_QbLMIUHH7wPE34/edit

#include "UnwrapMultiFrameImage.h"
#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
	int numFrames = 0;
	//Initialization:
	if (SUCCEEDED(CoInitialize(NULL)))
	{
		{
			// Call converting function, with the file-name to convert and a buffer to store the frame-count:
			bool res = UnwrapMultiFrameImage(_bstr_t(argv[1]), &numFrames);
			if (res)
			{
				if (numFrames > 0)
					cout << "Extracted " << numFrames << " images from file " << argv[1] << endl;
				else
					cout << argv[1] << " is not an Ennhanced (multfirame) MR Image File" << endl;
			}
			else
			{
				cout << "Error when processing file " << argv[1] << endl;
			}
		}
		CoUninitialize();
	}
	return numFrames;
}