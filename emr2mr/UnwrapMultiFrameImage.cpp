﻿#import "rzdcx.dll" rename_namespace ("rzdcxLib")
#include <iostream>
#include "UnwrapMultiFrameImage.h"

using namespace rzdcxLib;
using namespace std;

// Create the filename for the frame (zero based)
_bstr_t FrameFileName(BSTR filename, int i)
{
	// Create a name of the form: <original_file_name>_<frame_number>.dcm
	return _bstr_t(filename) + L"_" + _bstr_t(_variant_t(i)) + L".dcm";
}

// A nice little class for one frame
class FrameCreator {
	IDCXOBJPtr frame;
	int frameNumber;

	// Copy the content of the function groups per frame into the base 
	void CopyFunctionalGroups(IDCXOBJPtr item)
	{
		// We iterate through the CONTENTS of the Funcional-Group-Item associated with the current frame:
		IDCXELMIteratorPtr it = item->iterator();
		for (; !(it->AtEnd()); it->Next()) {
			IDCXELMPtr fge = it->Get();	// current func group
			cout << "Tag: " << fge->Tag << std::endl;
			
			// We don't copy private groups
			if (!((fge->Tag >> 16) % 2)) 
			{
				if (fge->Length > 0)
				{
					IDCXOBJIteratorPtr seq = fge->Value;
					if (seq) {
						IDCXOBJPtr item1 = seq->Get(); // There must be one!
						// it_single iterates through the only sequence-item of each functional group item (fge->seq)
						IDCXELMIteratorPtr it_single = item1->iterator();
						for (; !(it_single->AtEnd()); it_single->Next()) {
							if (!it_single->AtEnd()) {// sequence not empty
								IDCXELMPtr elem = it_single->Get();
								frame->insertElement(elem);	// insert the dicom-element to the new frame object (a dicom-object)
							}
						}
					}
				}
			}

			//	for (; !it_e->AtEnd(); it_e->Next) {
			//		IDCXELMPtr ->Get().Value
			//		IDCXOBJPtr dat(__uuidof(DCXOBJ));
			//		dat = it_e->Get()->Value;
			//		IDCXELMIteratorPtr it_data = dat->
			//	}
		}
	}
	
	// Do 
	void GiveMassage()
	{
		// Change number of frames property
		IDCXELMPtr e(__uuidof(DCXELM));
		e->Init((int)DICOM_TAGS_ENUM::NumberOfFrames);
		e->Value = 1;
		frame->insertElement(e);
	}
	unsigned int GetLenOfFrame(IDCXOBJPtr dcm)
	{
		return dcm->GetElement((int)DICOM_TAGS_ENUM::BitsAllocated)->Value.uiVal / 8 *
			dcm->GetElement((int)DICOM_TAGS_ENUM::Rows)->Value.uiVal *
			dcm->GetElement((int)DICOM_TAGS_ENUM::Columns)->Value.uiVal;

	}

	USHORT* FramePtr(IDCXELMPtr e, int frameNum, unsigned int frameLen)
	{
		// Good for both x86 and for x64 !!!
		__int64 framePtr(0);	// long long type, as required for GetRawDataEx
		long len;				// *Element* length
		e->GetRawDataEx(&framePtr, &len);
		// Cast 'back' to USHORT* , (so MSB zeros are cut away):
		USHORT* res = (USHORT*)(framePtr + frameNum * frameLen);
		return res;
	}

	void SetPixels(IDCXOBJPtr dcm, USHORT* framePtr)
	{
		unsigned int frameLength = GetLenOfFrame(dcm);
		IDCXELMPtr sfPixelData(__uuidof(DCXELM));
		sfPixelData->Init((int)DICOM_TAGS_ENUM::PixelData);

		//sfPixelData->Length = frameLength / 2;
		//sfPixelData->Value = (__int3264)(framePtr);

		//x86
#ifdef _X64
		sfPixelData->SetRawDataEx((long long)framePtr, frameLength);
#else
		sfPixelData->SetRawData((int)framePtr, frameLength);
#endif // _X64


		dcm->insertElement(sfPixelData);
	}
	
public:
	/// Constructor for FrameCreator class:
	/// Extracts a single frame from the enhanced MR in IDCXOBJPtr dcm,
	/// According to the int frameNumber,
	/// and using the per-Frame-Functional-Group-Sequence-Item IDCXOBJPtr perFrameItem
	FrameCreator(IDCXOBJPtr dcm, IDCXELMPtr mfPixelData, int frameNumber, IDCXOBJPtr sharedItem, IDCXOBJPtr perFrameItem)
	{
		frame = dcm;
		UINT frameLen(0);
		frameLen = GetLenOfFrame(dcm);
		USHORT* framePtr = FramePtr(mfPixelData, frameNumber, frameLen);

		//SetPixels(dcm, mfPixelData, frameNumber);
		SetPixels(dcm, framePtr);

		//Set the new attributes of the single-frame object:
		GiveMassage();
		CopyFunctionalGroups(sharedItem);
		CopyFunctionalGroups(perFrameItem);
	}
	void save(BSTR filename)
	{
		frame->saveFile(filename);
	}
};

/// bool UnwrapMultiFrameImage(/*IN*/BSTR filename, /*OUT*/int * numOfFrames)
/// gets a file-path [BSTR filename] and a buffer to hold frame-count [int * numOfFrames]
/// the function returns true for success and false for failiure
bool UnwrapMultiFrameImage(/*IN*/BSTR filename, /*OUT*/int* numOfFrames)
{
	*numOfFrames = 0;

	try
	{
		//Create a DICOM object:
		IDCXOBJPtr dcm(__uuidof(DCXOBJ));
		//Load object from given file:
		dcm->openFile(filename);
		
		// Get the pixels
		IDCXELMPtr pixelData = dcm->GetElement((int)DICOM_TAGS_ENUM::PixelData);
		dcm->RemoveElement((int)DICOM_TAGS_ENUM::PixelData);

		//Handle ONLY enhanced-MR object, according to it's SOP CLASS_UID - Ehance MR Image
		if (0 == wcscmp(L"1.2.840.10008.5.1.4.1.1.4.1", dcm->GetElement(DICOM_TAGS_ENUM::sopClassUid)->Value.bstrVal))
		{
			//Get frame-count:
			*numOfFrames = dcm->GetElement((int)DICOM_TAGS_ENUM::NumberOfFrames)->Value;

			// Change series instance UId:
			IDCXUIDPtr u(__uuidof(DCXUID));
			IDCXELMPtr e(__uuidof(DCXELM));
			e->Init((int)DICOM_TAGS_ENUM::seriesInstanceUID);
			e->Value = u->CreateUID(UID_TYPE::UID_TYPE_SERIES);
			dcm->insertElement(e);

			// Change SOP instance UId:
			e->Init((int)DICOM_TAGS_ENUM::sopInstanceUID);
			e->Value = u->CreateUID(UID_TYPE::UID_TYPE_INSTANCE);
			dcm->insertElement(e);

			// Change SOP Class UId:
			e->Init((int)DICOM_TAGS_ENUM::sopClassUid);
			e->Value = L"1.2.840.10008.5.1.4.1.​1.​4";
			dcm->insertElement(e);

			/// extract shared-functional-group-sequence:
			IDCXELMPtr sharedFuncGroupSeq(dcm->GetElement((int)DICOM_TAGS_ENUM::SharedFunctionalGroupsSequence));
			IDCXELMPtr perFrameFuncGroupSeq(dcm->GetElement((int)DICOM_TAGS_ENUM::PerFrameFunctionalGroupsSequence));

			dcm->RemoveElement((int)DICOM_TAGS_ENUM::SharedFunctionalGroupsSequence);
			dcm->RemoveElement((int)DICOM_TAGS_ENUM::PerFrameFunctionalGroupsSequence);
			dcm->RemoveElement((int)DICOM_TAGS_ENUM::InstanceNumber);

			IDCXOBJPtr sharedFuncGroups;
			{
				IDCXOBJIteratorPtr it = sharedFuncGroupSeq->Value;
				for ( ; !it->AtEnd(); it->Next())
				{
					sharedFuncGroups = it->Get();
					break; // We just take the first
				}
			}

			/// Now go over every frames
			{
				/// each iteration it gets value of a single item from list (Item[1],Item[2],Item[i],...)
				IDCXOBJIteratorPtr it = perFrameFuncGroupSeq->Value;
				for (int i = 0; !it->AtEnd(); it->Next(), i++)
				{
					IDCXOBJPtr perFrameFuncGroups = it->Get();	//Item corresponding to current frame from PerFrameFunctionalGroup sequence
					
					// Create a single-frame object and save it:
					FrameCreator(dcm, pixelData, i, sharedFuncGroups, perFrameFuncGroups).save(FrameFileName(filename, i));
				}
				return true;	//Success
			}
		}
		else
		{
			*numOfFrames = -1;
			return true;	//Not an enhanced-MR object, therefore failiure
		}
	}
	catch (_com_error & e)
	{
		cout << "Error when processing file " << filename << ", message: " << e.Description().operator wchar_t *() << endl;
		return false;
	}
}
